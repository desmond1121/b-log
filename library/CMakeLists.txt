# Sets the minimum version of CMake required to build the native
# library. You should either keep the default value or only pass a
# value of 3.4.0 or lower.

cmake_minimum_required(VERSION 3.4.1)

# Creates and names a library, sets it as either STATIC
# or SHARED, and provides the relative paths to its source code.
# You can define multiple libraries, and CMake builds it for you.
# Gradle automatically packages shared libraries with your APK.


set(BASE src/main/cpp)
find_library(log-lib log)
find_library(z-lib z)

include_directories(${BASE}/includes)

file(GLOB SOURCE "${BASE}/*.cpp" "${BASE}/utils/*.cpp")

add_library(blog-lib SHARED ${SOURCE})
target_link_libraries(blog-lib ${log-lib} ${z-lib})

