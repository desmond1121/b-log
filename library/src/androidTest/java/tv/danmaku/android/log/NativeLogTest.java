/*
 * Copyright (c) 2016. bilibili
 */

package tv.danmaku.android.log;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import static junit.framework.Assert.*;

@RunWith(AndroidJUnit4.class)
public class NativeLogTest {

    static {
        System.loadLibrary("blog");
    }

    private NativeLogger nativeLogger;
    private FileManager fileManager;

    @Before
    public void setUp() throws Exception {
        Context context = InstrumentationRegistry.getContext();
        File logDir = context.getExternalFilesDir("blog-test");
        InternalUtils.clean(logDir);

        LogSetting setting = new LogSetting.Builder(context)
                .setLogDir(logDir.getAbsolutePath())
                .setDebuggable(true)
                .build();
        nativeLogger = new NativeLogger(setting, false);
        fileManager = new FileManager(setting);
    }

    @After
    public void tearDown() throws Exception {
        nativeLogger.teardown();
    }

    @Test
    public void testSimpleLog() {
        int lineNumber = 8096;
        for (int i = 0; i < lineNumber; i++) {
            nativeLogger.log(LogPriority.DEBUG, String.valueOf(i), "无可奉告成天就想搞个大新闻小拳拳捶你胸");
        }
        nativeLogger.flush();
        File file = fileManager.currentOutputFile(LogSetting.LOG);
        assertEquals(TestUtils.lineCount(file), lineNumber + 1); // we add a ___BEGIN___LOG___ at first
    }

    @Test
    public void testSimpleEvent() {
        int lineNumber = 8096;
        for (int i = 0; i < lineNumber; i++) {
            nativeLogger.event(String.valueOf(i), "无可奉告成天就想搞个大新闻小拳拳捶你胸");
        }
        nativeLogger.flush();
        File file = fileManager.currentOutputFile(LogSetting.EVENT);
        assertEquals(TestUtils.lineCount(file), lineNumber + 1);
    }

    @Test
    public void testMultiThread() throws InterruptedException, IOException {
        final CountDownLatch countDownLatch = new CountDownLatch(4);

        new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while (i < 4096) {
                    nativeLogger.log(LogPriority.DEBUG, "thread 1", "做了一点小成绩");
                    i++;
                }
                countDownLatch.countDown();
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while (i < 4096) {
                    nativeLogger.log(LogPriority.DEBUG, "thread 2", "无可奉告");
                    i++;
                }
                countDownLatch.countDown();
            }
        }).start();

        /* event */
        new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while (i < 4096) {
                    nativeLogger.event("thread 3", "成天就想搞个大新闻");
                    i++;
                }
                countDownLatch.countDown();
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while (i < 4096) {
                    nativeLogger.event("thread 4", "谈笑风生");
                    i++;
                }
                countDownLatch.countDown();
            }
        }).start();

        countDownLatch.await();
        nativeLogger.flush();

        File logFile = fileManager.currentOutputFile(LogSetting.LOG);
        assertEquals(TestUtils.lineCount(logFile), 8192 + 1);

        File eventFile = fileManager.currentOutputFile(LogSetting.EVENT);
        assertEquals(TestUtils.lineCount(eventFile), 8192 + 1);
    }
}
