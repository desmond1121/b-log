package tv.danmaku.android.log;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created by desmond on 2/4/17.
 */
@RunWith(AndroidJUnit4.class)
public class InitializeTest {

    static {
        System.loadLibrary("blog");
    }

    @Test
    public void testInitialize() {
        Context context = InstrumentationRegistry.getContext();
        BLog.initialize(context);
        LogSetting setting = BLog.getSetting();

        assertNotNull(setting);
        assertTrue(BLog.getLogDir().exists());
        assertTrue(setting.getExpiredDay() == 2);

        BLog.shutdown();
    }

    @Test
    public void testInitializeWithConfig() throws IOException {
        Context context = InstrumentationRegistry.getContext();
        File logDir = context.getExternalFilesDir("test_log");
        if (logDir == null)
            logDir = context.getDir("test_log", Context.MODE_PRIVATE);

        logDir.createNewFile();

        LogSetting setting = new LogSetting.Builder(context)
                .setDefaultTag("TEST")
                .setLogDir(logDir.getPath())
                .setExpiredDay(1)
                .setLogcatPriority(LogPriority.DEBUG)
                .setLogfilePriority(LogPriority.INFO)
                .build();

        BLog.initialize(setting);
        setting = BLog.getSetting();

        assertNotNull(setting);
        assertTrue(new File(setting.getLogDir()).exists());
        assertTrue(setting.getLogDir().equals(logDir.getAbsolutePath()));
        assertTrue(setting.getDefaultTag().equals("TEST"));
        assertTrue(setting.getExpiredDay() == 1);
        assertTrue(setting.getLogcatPriority() == LogPriority.DEBUG);
        assertTrue(setting.getLogfilePriority() == LogPriority.INFO);

        BLog.shutdown();
    }
}
