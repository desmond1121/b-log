package tv.danmaku.android.log;

import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

/**
 * Created by desmond on 2/4/17.
 */
public class TestUtils {
    private static final String TAG = "BLOG_DEBUG";

    static int lineCount(File file) {
        LineNumberReader lnr = null;
        try {
            lnr = new LineNumberReader(new FileReader(file));
            lnr.skip(Long.MAX_VALUE);
            return lnr.getLineNumber();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            InternalUtils.closeQuietly(lnr);
        }
        return -1;
    }

    static void cat(File file) {
        BufferedReader br;
        Log.d(TAG, "----CAT FILE [" + file + "]----");
        try {
            br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                Log.d(TAG, line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "----CAT FILE END ----");
    }

}
