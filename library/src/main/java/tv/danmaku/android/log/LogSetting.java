/*
 * Copyright (c) 2016. bilibili
 */

package tv.danmaku.android.log;

import android.content.Context;
import android.os.Environment;
import android.support.annotation.IntRange;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;

/**
 * BLog config class, use {@link LogSetting.Builder} to custom your config.
 *
 * @author kaede
 * @version date 16/9/22
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class LogSetting {

    static final String TAG = "blog";
    private static final String DEFAULT_DIR_OLD = "blog";
    private static final String DEFAULT_DIR = "blog_v2";

    // QUERY MODE
    public static final int LOG = 0x0001;
    public static final int EVENT = 0x0010;

    private int mExpiredDay;
    private int mLogcatPriority;
    private int mLogfilePriority;
    private boolean mDebuggable;
    private String mLogDir;
    private String mDefaultTag;

    private LogSetting() {
    }

    /**
     * Get level to check whether to logcat or not.
     */
    public int getLogcatPriority() {
        return mLogcatPriority;
    }

    /**
     * Get level to check whether to log file or not.
     */
    public int getLogfilePriority() {
        return mLogfilePriority;
    }

    /**
     * Get log files' base dir.
     */
    public String getLogDir() {
        return mLogDir;
    }

    public int getExpiredDay() {
        return mExpiredDay;
    }

    public String getDefaultTag() {
        return mDefaultTag;
    }

    public boolean debuggable() {
        return mDebuggable;
    }

    public static class Builder {
        private Context mContext;
        private int mExpiredDay;
        private int mLogcatPriority = -1;
        private int mLogfilePriority = -1;
        private String mLogDir;
        private String mDefaultTag;
        private boolean mDebuggable;

        public Builder(Context context) {
            mContext = context;
            mExpiredDay = 2;
            mDefaultTag = "BLOG";
            mDebuggable = BuildConfig.DEBUG;
        }

        public Builder setLogDir(String path) {
            if (!TextUtils.isEmpty(path)) {
                mLogDir = path;
            }
            return this;
        }

        public Builder setDefaultTag(String defaultTag) {
            this.mDefaultTag = defaultTag;
            return this;
        }


        public Builder setLogcatPriority(@IntRange(from = LogPriority.VERBOSE, to = LogPriority.NONE)
                                                 int priority) {
            if (LogPriority.isValid(priority)) {
                mLogcatPriority = priority;
            }
            return this;
        }

        public Builder setLogfilePriority(@IntRange(from = LogPriority.VERBOSE, to = LogPriority.NONE)
                                                  int priority) {
            if (LogPriority.isValid(priority)) {
                mLogfilePriority = priority;
            }
            return this;
        }

        public Builder setExpiredDay(int expiredDay) {
            if (mExpiredDay > 0) {
                mExpiredDay = expiredDay;
            }
            return this;
        }

        public Builder setDebuggable(boolean debuggable) {
            mDebuggable = debuggable;
            return this;
        }

        public LogSetting build() {
            LogSetting setting = new LogSetting();
            setting.mLogDir = mLogDir;
            setting.mDebuggable = mDebuggable;
            setting.mExpiredDay = mExpiredDay;
            setting.mLogcatPriority = mLogcatPriority;
            setting.mLogfilePriority = mLogfilePriority;
            setting.mDefaultTag = mDefaultTag;

            if (setting.mLogcatPriority == -1) {
                setting.mLogcatPriority = mDebuggable ? LogPriority.VERBOSE : LogPriority.ERROR;
            }
            if (setting.mLogfilePriority == -1) {
                setting.mLogfilePriority = mDebuggable ? LogPriority.DEBUG : LogPriority.INFO;
            }

            if (TextUtils.isEmpty(setting.mLogDir)) {
                ensureLogDir(setting);
            }

            return setting;
        }

        private void ensureLogDir(LogSetting setting) {
            File rootDir = null;
            boolean external = Environment.MEDIA_MOUNTED
                    .equals(Environment.getExternalStorageState());

            if (external) {
                try {
                    /* 清除旧版本blog的文件夹 */
                    File oldDir = mContext.getExternalFilesDir(DEFAULT_DIR_OLD);
                    if (oldDir != null) {
                        InternalUtils.deleteQuietly(oldDir);
                    }

                    rootDir = mContext.getExternalFilesDir(DEFAULT_DIR);
                } catch (Throwable e) {
                    if (mDebuggable) {
                        e.printStackTrace();
                    }
                }
            }

            if (rootDir == null) {
                if (mDebuggable) {
                    Log.w(TAG, "create external log dir fail, do you miss the permission?");
                }
                rootDir = mContext.getDir(DEFAULT_DIR, Context.MODE_PRIVATE);
            }

            InternalUtils.createDir(rootDir);
            setting.mLogDir = rootDir.getAbsolutePath();
        }
    }
}
