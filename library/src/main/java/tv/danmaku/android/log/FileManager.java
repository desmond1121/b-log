package tv.danmaku.android.log;

import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import java.io.File;
import java.io.FilenameFilter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 *  //todo: Implement in cpp.
 *
 * Created by desmond on 2/4/17.
 */
/* package */ class FileManager implements IFileManager {

    private static final SimpleDateFormat mNameFormatter = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
    private static final String ZIP_EXT = "zip";
    private static final String FILE_HYPHEN = "-";
    private LogSetting mSetting;

    FileManager(LogSetting mSetting) {
        this.mSetting = mSetting;
    }

    /**
     * Used for test
     */
    public File currentOutputFile(int mode) {
        final String date = mNameFormatter.format(new Date(System.currentTimeMillis()));
        String fname;
        switch (mode) {
            case LogSetting.EVENT:
                fname = date + "-" + InternalUtils.getProcessName() + "." + NativeLogger.getEventExt();
                break;
            case LogSetting.LOG:
                fname = date + "-" + InternalUtils.getProcessName() + "." + NativeLogger.getLogExt();
                break;
            case LogSetting.LOG | LogSetting.EVENT:
            default:
                throw new IllegalStateException("mode should be LogSetting.MODE_EVENT or LogSetting.MODE_LOG");
        }
        return new File(mSetting.getLogDir(), fname);
    }

    @Nullable
    @Override
    public File[] queryFiles(final int mode, final long dateMs) {
        final String date = mNameFormatter.format(new Date(dateMs));
        File folder = new File(mSetting.getLogDir());

        if (folder.exists() && folder.isDirectory()) {
            return folder.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String filename) {
                    switch (mode) {
                        case LogSetting.EVENT:
                            return filename.startsWith(date) && filename.endsWith(NativeLogger.getEventExt());
                        case LogSetting.LOG:
                            return filename.startsWith(date) && filename.endsWith(NativeLogger.getLogExt());
                        case LogSetting.LOG | LogSetting.EVENT:
                        default:
                            return filename.startsWith(date) &&
                                    (filename.endsWith(NativeLogger.getLogExt()) || filename.endsWith(NativeLogger.getEventExt()));
                    }
                }
            });
        }
        return null;
    }

    @Nullable
    @Override
    public File[] queryFiles(final int mode) {
        File folder = new File(mSetting.getLogDir());
        if (folder.exists() && folder.isDirectory()) {
            return folder.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String filename) {
                    switch (mode) {
                        case LogSetting.EVENT:
                            return filename.endsWith(NativeLogger.getEventExt());
                        case LogSetting.LOG:
                            return filename.endsWith(NativeLogger.getLogExt());
                        case LogSetting.LOG | LogSetting.EVENT:
                        default:
                            return filename.endsWith(NativeLogger.getLogExt()) || filename.endsWith(NativeLogger.getEventExt());
                    }
                }
            });
        }
        return null;
    }

    /**
     * @param dateMs -1 if zip all file, or some positive value for specific date
     */
    @VisibleForTesting
    String getZipPath(int mode, long dateMs) {
        String fname;
        String dateStr;
        if (dateMs > 0) {
            dateStr = mNameFormatter.format(new Date(dateMs));
        } else {
            dateStr = "all";
        }

        fname = new StringBuilder(dateStr)
                .append(FILE_HYPHEN)
                .append(Thread.currentThread().getId())
                .append(FILE_HYPHEN)
                .append(mode)
                .append(".zip").toString();

        return mSetting.getLogDir() + File.separator + fname;
    }

    @Override
    public File zippingFiles(int mode, @Nullable List<File> attaches) {
        return zippingFiles(mode, -1, attaches);
    }

    @Override
    public File zippingFiles(int mode, long date, @Nullable List<File> attaches) {
        File zipFile = new File(getZipPath(mode, date));
        InternalUtils.deleteQuietly(zipFile);
        List<File> filesForZip;

        File[] files = queryFiles(mode);
        if (files != null) {
            filesForZip = Arrays.asList(files);
        } else {
            filesForZip = new ArrayList<>();
        }

        if (attaches != null) {
            filesForZip.addAll(attaches);
        }

        if (InternalUtils.zippingFiles(filesForZip, zipFile)) {
            return zipFile;
        }
        return null;
    }

    @Override
    public List<String> cleanExpiredLogs() {

        File folder = new File(mSetting.getLogDir());
        if (folder.exists() && folder.isDirectory()) {
            File[] allFiles = folder.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String fileName) {
                    return fileName.endsWith(NativeLogger.getLogExt())
                                    || fileName.endsWith(NativeLogger.getEventExt())
                                    || fileName.endsWith(ZIP_EXT);
                }
            });

            List<String> list = new ArrayList<>();
            for (File file : allFiles) {
                String fileName = file.getName();
                String fileDateInfo = getFileDate(fileName);

                if (fileDateInfo == null) {
                    continue;
                }

                if (isExpired(fileDateInfo)) {
                    list.add(file.getAbsolutePath());
                    InternalUtils.deleteQuietly(file);
                }
            }
            return list;
        }

        return null;
    }

    private boolean isExpired(String dateStr) {
        boolean canDel;
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -1 * mSetting.getExpiredDay());
        Date expiredDate = calendar.getTime();
        try {
            Date createDate = mNameFormatter.parse(dateStr);
            canDel = createDate.before(expiredDate);
        } catch (ParseException e) {
            canDel = false;
        }
        return canDel;
    }

    private String getFileDate(String fileName) {
        int index = fileName.indexOf(FILE_HYPHEN);
        if (index == -1) return null;

        return fileName.substring(0, index);
    }
}
