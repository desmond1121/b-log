/*
 * Copyright (c) 2016. bilibili
 */

package tv.danmaku.android.log;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.annotation.WorkerThread;
import android.util.Log;

import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * BLog is an Android LogCat extended Utility. It can simplify the way you use
 * {@link android.util.Log}, as well as write our log message into file for after support.
 *
 * @author kaede
 * @version date 16/9/22
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class BLog {
    private static Logger sLogger;

    private BLog() {
    }

    private static boolean checkInit() {
        boolean init = sLogger != null;

        if (!init) {
            Log.w(LogSetting.TAG, "pls call Blog.initialize first!");
        }

        return init;
    }

    /**
     * You should call {@link BLog#initialize(Context)} before using BLog.
     */
    public static void initialize(Context context) {
        if (context == null) return;

        initialize(new LogSetting.Builder(context).build());
    }

    /**
     * You should call {@link BLog#initialize(LogSetting)} before using BLog.
     *
     * @param setting Custom config
     */
    public static void initialize(LogSetting setting) {
        if (setting == null) return;

        if (sLogger == null) {
            synchronized (BLog.class) {
                if (sLogger == null) {
                    sLogger = new Logger(setting);
                }
            }
        }
    }

    /**
     * You should call {@link BLog#shutdown()} before you call
     * {@link BLog#initialize(Context)} again.
     */
    public static void shutdown() {
        if (checkInit()) {
            sLogger.flushIO();
            sLogger.teardown();
            sLogger = null;
        }
    }

    /**
     * verbose
     **/
    public static void v(String message) {
        if (checkInit()) {
            sLogger.verbose(null, message);
        }
    }

    public static void v(String tag, String message) {
        if (checkInit()) {
            sLogger.verbose(tag, message);
        }
    }

    public static void v(String message, Throwable throwable) {
        if (checkInit()) {
            sLogger.verbose(null, throwable, message);
        }
    }

    public static void v(String tag, String message, Throwable throwable) {
        if (checkInit()) {
            sLogger.verbose(tag, throwable, message);
        }
    }

    public static void vfmt(String tag, String fmt, Object... args) {
        if (checkInit()) {
            sLogger.verbose(tag, fmt, args);
        }
    }

    /**
     * debug
     **/
    public static void d(String message) {
        if (checkInit()) {
            sLogger.debug(null, message);
        }
    }

    public static void d(String tag, String message) {
        if (checkInit()) {
            sLogger.debug(tag, message);
        }
    }

    public static void d(String message, Throwable throwable) {
        if (checkInit()) {
            sLogger.debug(null, throwable, message);
        }
    }

    public static void d(String tag, String message, Throwable throwable) {
        if (checkInit()) {
            sLogger.debug(tag, throwable, message);
        }
    }

    public static void dfmt(String tag, String fmt, Object... args) {
        if (checkInit()) {
            sLogger.debug(tag, fmt, args);
        }
    }

    /**
     * info
     **/
    public static void i(String message) {
        if (checkInit()) {
            sLogger.info(null, message);
        }
    }

    public static void i(String tag, String message) {
        if (checkInit()) {
            sLogger.info(tag, message);
        }
    }

    public static void i(String message, Throwable throwable) {
        if (checkInit()) {
            sLogger.verbose(null, throwable, message);
        }
    }

    public static void i(String tag, String message, Throwable throwable) {
        if (checkInit()) {
            sLogger.info(tag, throwable, message);
        }
    }

    public static void ifmt(String tag, String fmt, Object... args) {
        if (checkInit()) {
            sLogger.info(tag, fmt, args);
        }
    }

    /**
     * warning
     **/
    public static void w(String message) {
        if (checkInit()) {
            sLogger.warn(null, message);
        }
    }

    public static void w(String tag, String message) {
        if (checkInit()) {
            sLogger.warn(tag, message);
        }
    }

    public static void w(String message, Throwable throwable) {
        if (checkInit()) {
            sLogger.warn(null, throwable, message);
        }
    }

    public static void w(String tag, String message, Throwable throwable) {
        if (checkInit()) {
            sLogger.warn(tag, throwable, message);
        }
    }

    public static void wfmt(String tag, String fmt, Object... args) {
        if (checkInit()) {
            sLogger.warn(tag, fmt, args);
        }
    }

    /**
     * warning
     **/
    public static void e(String message) {
        if (checkInit()) {
            sLogger.error(null, message);
        }
    }

    public static void e(String tag, String message) {
        if (checkInit()) {
            sLogger.error(tag, message);
        }
    }

    public static void e(String message, Throwable throwable) {
        if (checkInit()) {
            sLogger.error(null, throwable, message);
        }
    }

    public static void e(String tag, String message, Throwable throwable) {
        if (checkInit()) {
            sLogger.error(tag, throwable, message);
        }
    }

    public static void efmt(String tag, String fmt, Object... args) {
        if (checkInit()) {
            sLogger.error(tag, fmt, args);
        }
    }

    /**
     * wtf
     **/
    public static void wtf(String message) {
        if (checkInit()) {
            sLogger.wtf(null, message);
        }
    }

    public static void wtf(String tag, String message) {
        if (checkInit()) {
            sLogger.wtf(tag, message);
        }
    }

    public static void wtffmt(String tag, String fmt, Object... args) {
        if (checkInit()) {
            sLogger.wtf(tag, fmt, args);
        }
    }

    /**
     * ILogger event, logging message in an unique file.
     * Note that this api will log message in logcat according to {@link LogSetting#getEventPriority()}.
     **/
    public static void event(String message) {
        if (checkInit()) {
            sLogger.event(null, message);
        }
    }

    /**
     * See {@linkplain #event(String)}.
     **/
    public static void event(String tag, String message) {
        if (checkInit()) {
            sLogger.event(tag, message);
        }
    }

    /**
     * Log message to file, and it would flush contents to disk immediately.
     * Note: It would block thread and wait I/O success.
     */
    @WorkerThread
    public static void syncLog(int priority, String message) {
        if (checkInit()) {
            sLogger.syncLog(priority, null, message);
        }
    }

    /**
     * See {@linkplain #syncLog(int, String)}.
     **/
    @WorkerThread
    public static void syncLog(int priority, String tag,  String message) {
        if (checkInit()) {
            sLogger.syncLog(priority, tag, message);
        }
    }

    /**
     * get all log files
     *
     * @param mode mode for filtering log files, support '|' operation,
     *             see {@link LogSetting#MODE_LOG}, {@link LogSetting#MODE_EVENT}
     */
    public static File[] getLogFiles(int mode) {
        if (checkInit()) {
            return sLogger.queryFiles(mode);
        }
        return null;
    }

    /**
     * get log files by day
     *
     * @param date retain null if today
     */
    public static File[] getLogFilesByDate(int mode, Date date) {
        if (checkInit()) {
            if (date == null) {
                date = new Date(); // today
            }
            return sLogger.queryFiles(mode, date.getTime());
        }
        return null;
    }

    @Deprecated
    @WorkerThread
    public static File zippingLogFiles(int mode) {
        return zippingLogFiles(mode, null);
    }

    @Deprecated
    @WorkerThread
    public static File zippingLogFilesByDate(int mode, Date date) {
        return zippingLogFilesByDate(mode, date, null);
    }

    /**
     * Zipping log files and return the zip file.
     */
    @WorkerThread
    public static File zippingLogFiles(int mode, @Nullable List<File> attachs) {
        if (checkInit()) {
            return sLogger.zippingFiles(mode, attachs);
        }
        return null;
    }

    @WorkerThread
    public static File zippingLogFilesByDate(int mode, Date date, @Nullable List<File> attachs) {
        if (checkInit()) {
            if (date == null) {
                date = new Date(); // today
            }
            return sLogger.zippingFiles(mode, date.getTime(), attachs);
        }
        return null;
    }

    /**
     * Get log file's directory.
     */
    public static File getLogDir() {
        if (checkInit()) {
            return new File(sLogger.getSetting().getLogDir());
        }
        return null;
    }

    /**
     * Delete existing log files.
     */
    public static void deleteLogs() {
        if (checkInit()) {
            InternalUtils.deleteQuietly(new File(sLogger.getSetting().getLogDir()));
        }
    }

    @VisibleForTesting
    static Logger getLogger() {
        return sLogger;
    }

    /**
     * Package accessible for testcase.
     */
    static LogSetting getSetting() {
        if (checkInit()) {
            return sLogger.getSetting();
        }
        return null;
    }
}
