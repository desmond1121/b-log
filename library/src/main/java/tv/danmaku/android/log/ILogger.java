/*
 * Copyright (c) 2016. bilibili
 */

package tv.danmaku.android.log;

interface ILogger {

    /**
     * do log.
     */
    void log(int priority, String tag, String msg);

    /**
     * event (do not have priority, independent file).
     */
    void event(String tag, String msg);

    /**
     * flush log to destination.
     */
    void flush();

    /**
     * release resources.
     */
    void teardown();

}
