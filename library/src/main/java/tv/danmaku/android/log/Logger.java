/*
 * Copyright (c) 2016. bilibili
 */

package tv.danmaku.android.log;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

class Logger {

    private final String mDefaultTag;
    private final LogSetting mSetting;
    private final IFileManager mFileManager;
    private final List<ILogger> mLoggers;

    /* package */ Logger(LogSetting setting) {
        mSetting = setting;
        mDefaultTag = setting.getDefaultTag();
        mLoggers = new ArrayList<>();

        if (setting.getLogcatPriority() != LogPriority.NONE) {
            mLoggers.add(new AndroidLogger(setting));
        }

        if (setting.getLogfilePriority() != LogPriority.NONE) {
            mLoggers.add(new NativeLogger(setting, true));
        }
        mFileManager = new FileManager(setting);
        cleanExpiredFiles();
    }

    /* package */ void teardown() {
        for (ILogger logger : mLoggers) {
            logger.teardown();
        }
    }

    /* package */ void cleanExpiredFiles() {
        Executor.instance().post(new Runnable() {
            @Override
            public void run() {
                try {
                    mFileManager.cleanExpiredLogs();
                } catch (Throwable e) {
                    if (mSetting.debuggable()) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    /**
     * verbose
     **/
    /* package */ void verbose(String tag, String fmt, Object... args) {
        logInternal(LogPriority.VERBOSE, ensureTag(tag), formatMessage(fmt, args));
    }

    /* package */ void verbose(String tag, Throwable throwable, String message) {
        logInternal(LogPriority.VERBOSE, ensureTag(tag), formatThrowable(message, throwable));
    }

    /**
     * debug
     **/
    /* package */ void debug(String tag, String fmt, Object... args) {
        logInternal(LogPriority.DEBUG, ensureTag(tag), formatMessage(fmt, args));
    }

    /* package */ void debug(String tag, Throwable throwable, String message) {
        logInternal(LogPriority.DEBUG, ensureTag(tag), formatThrowable(message, throwable));
    }

    /**
     * info
     **/
    /* package */ void info(String tag, String fmt, Object... args) {
        logInternal(LogPriority.INFO, ensureTag(tag), formatMessage(fmt, args));
    }

    /* package */ void info(String tag, Throwable throwable, String message) {
        logInternal(LogPriority.INFO, ensureTag(tag), formatThrowable(message, throwable));
    }

    /**
     * warning
     **/
    /* package */ void warn(String tag, String fmt, Object... args) {
        logInternal(LogPriority.WARN, ensureTag(tag), formatMessage(fmt, args));
    }

    /* package */ void warn(String tag, Throwable throwable, String message) {
        logInternal(LogPriority.WARN, ensureTag(tag), formatThrowable(message, throwable));
    }

    /**
     * error
     **/
    /* package */ void error(String tag, String fmt, Object... args) {
        logInternal(LogPriority.ERROR, ensureTag(tag), formatMessage(fmt, args));
    }

    /* package */ void error(String tag, Throwable throwable, String message) {
        logInternal(LogPriority.ERROR, ensureTag(tag), formatThrowable(message, throwable));
    }

    /**
     * wtf
     **/
    /* package */ void wtf(String tag, String fmt, Object... args) {
        logInternal(LogPriority.ASSERT, ensureTag(tag), formatMessage(fmt, args));
    }

    /* package */ void syncLog(int priority, String tag, String message) {
        logInternal(priority, ensureTag(tag), message);
        flushIO();
    }

    /* package */ void event(String tag, String message) {
        for (ILogger logger : mLoggers) {
            logger.event(tag, message);
        }
    }

    private String ensureTag(String tag) {
        return TextUtils.isEmpty(tag) ? mDefaultTag : tag;
    }

    private String formatMessage(String fmt, Object... args) {
        if (args == null) return fmt;

        String message;
        try {
            message = String.format(fmt, args);
            return message;

        } catch (Throwable e) {
            if (mSetting.debuggable()) e.printStackTrace();

            StringBuilder sb = new StringBuilder("format error, fmt = " + String.valueOf(fmt)
                    + ", args = ");
            for (int i = 0; i < args.length; i++) {
                Object item = args[i];
                sb.append(String.valueOf(item));
                if (i != (args.length - 1)) sb.append(", ");
            }

            return sb.toString();
        }
    }

    private String formatThrowable(String message, Throwable throwable) {
        return String.valueOf(message) + " : "
                + (throwable == null ? "null" : InternalUtils.getStackTraceString(throwable));
    }

    private void logInternal(int priority, String tag, String message) {
        for (ILogger logger : mLoggers) {
            logger.log(priority, tag, message);
        }
    }

    /* package */ void flushIO() {
        for (ILogger logger : mLoggers) {
            logger.flush();
        }
    }

    public LogSetting getSetting() {
        return mSetting;
    }

    /* package */ File[] queryFiles(int mode) {
        if (mFileManager != null) {
            return mFileManager.queryFiles(mode);
        }
        return null;
    }

    /* package */ File[] queryFiles(int mode, long dateInMs) {
        if (mFileManager != null) {
            return mFileManager.queryFiles(mode, dateInMs);
        }
        return null;
    }

    /* package */ File zippingFiles(int mode, @Nullable List<File> attachs) {
        if (mFileManager != null) {
            return mFileManager.zippingFiles(mode, attachs);
        }
        return null;
    }

    /* package */ File zippingFiles(int mode, long dateInMs, @Nullable List<File> attachs) {
        if (mFileManager != null) {
            return mFileManager.zippingFiles(mode, dateInMs, attachs);
        }
        return null;
    }
}
