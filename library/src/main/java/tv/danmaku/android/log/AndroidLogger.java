/*
 * Copyright (c) 2016. bilibili
 */

package tv.danmaku.android.log;

import android.text.TextUtils;

class AndroidLogger implements ILogger {

    private static final int CHUNK_SIZE = 4000;

    private static final String EMPTY_MESSAGE = "EMPTY/NULL";
    private final LogSetting mSetting;

    /* package */ AndroidLogger(LogSetting setting) {
        mSetting = setting;
    }

    @Override
    public void log(int priority, String tag, String msg) {
        if (mSetting.getLogcatPriority() == LogPriority.NONE || mSetting.getLogcatPriority() > priority) {
            return;
        }

        // AndroidLogcat may abort long message, just in case.
        separateMessageIfNeed(priority, tag, msg);
    }

    @Override
    public void event(String tag, String msg) {
        android.util.Log.d("BLOG-EVENT-" + tag, msg);
    }

    @Override
    public void flush() {
        // do nothing
    }

    @Override
    public void teardown() {
        // do nothing
    }

    private void separateMessageIfNeed(int priority, String tag, String msg) {
        if (TextUtils.isEmpty(msg)) {
            logMessage(priority, tag, EMPTY_MESSAGE);
            return;
        }

        byte[] bytes = msg.getBytes();
        int length = bytes.length;

        if (length <= CHUNK_SIZE) {
            logMessage(priority, tag, msg);
            return;
        }

        for (int i = 0; i < length; i += CHUNK_SIZE) {
            int count = Math.min(length - i, CHUNK_SIZE);
            logMessage(priority, tag, new String(bytes, i, count));
        }
    }

    private void logMessage(int priority, String tag, String chunk) {
        String[] lines = chunk.split(System.getProperty("line.separator"));
        for (String line : lines) {
            line = "[" + Thread.currentThread().getName() + "]  " + line;
            logcat(priority, tag, line);
        }
    }

    private void logcat(int priority, String tag, String chunk) {
        android.util.Log.println(priority, tag, chunk);
    }
}
