package tv.danmaku.android.log;

import android.support.annotation.Nullable;

import java.io.File;
import java.io.FilenameFilter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * File utils.
 *
 * //todo: Implement in cpp, now use java.
 *
 * Created by desmond on 2/4/17.
 */
/* package */ interface IFileManager {

    @Nullable
    File[] queryFiles(final int mode, final long dateMs);

    @Nullable
    File[] queryFiles(final int mode);

    /**
     * zip all files.
     */
    @Nullable
    File zippingFiles(int mode, @Nullable List<File> attaches);

    /**
     * zip specific date files.
     * @param date -1 if zip all file, or some positive value for specific date
     */
    @Nullable
    File zippingFiles(int mode, long date, @Nullable List<File> attaches);

    /**
     * @return cleaned files' path
     */
    @Nullable
    List<String> cleanExpiredLogs();
}
