/*
 * Copyright (c) 2016. bilibili
 */

package tv.danmaku.android.log;

import android.os.Process;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author kaede
 * @version date 16/9/23
 */
class InternalUtils {

    private static final int BUFFER_SIZE = 4096;
    private static String sProcessName = null;

    static String getProcessName() {
        if (sProcessName == null) {
            sProcessName = getProcessNameInternal();
        }
        return sProcessName;
    }

    private static String getProcessNameInternal() {
        int pid = Process.myPid();
        StringBuffer cmdline = new StringBuffer();
        InputStream is = null;

        try {
            is = new FileInputStream("/proc/" + pid + "/cmdline");
            for (; ; ) {
                int c = is.read();
                if (c < 0)
                    break;
                cmdline.append((char) c);
            }
        } catch (Exception ignore) {
        } finally {
            closeQuietly(is);
        }

        String pname = cmdline.toString().trim();
        if (!pname.contains(":")) {
            return "main";

        } else {
            return pname.substring(pname.indexOf(":") + 1);
        }
    }

    public static void createDir(File dir) {
        if (!dir.isDirectory()) {
            dir.delete();
        }

        if (!dir.exists()) {
            dir.mkdirs();
        }
    }

    static String ensureSeparator(String path) {
        if (path.charAt(path.length() - 1) == File.separatorChar) {
            return path;
        }
        return path + File.separator;
    }

    static void clean(File file) {
        if (file.isDirectory()) {
            File[] list = file.listFiles();
            if (list != null) {
                for (File f : list) {
                    deleteQuietly(f);
                }
            }
        }
    }

    static void deleteQuietly(File file) {
        if (file != null) {
            if (file.isDirectory()) {
                File[] list = file.listFiles();
                if (list != null) {
                    for (File f : list) {
                        deleteQuietly(f);
                    }
                }
                file.delete();
            } else {
                file.delete();
            }
        }
    }

    static void closeQuietly(Closeable closeable) {
        if (closeable == null) {
            return;
        }
        try {
            closeable.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void copyStream(InputStream is, OutputStream os) {
        byte[] buffer = new byte[BUFFER_SIZE];
        try {
            int size = 0;
            while ((size = is.read(buffer)) > 0) {
                os.write(buffer, 0, size);
            }
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static String getStackTraceString(Throwable tr) {
        return android.util.Log.getStackTraceString(tr);
    }

    static boolean zippingFiles(List<File> files, File output) {
        if (files == null || files.size() == 0) return false;

        BufferedInputStream in = null;
        ZipOutputStream out = null;
        try {
            FileOutputStream fo = new FileOutputStream(output);
            out = new ZipOutputStream(new BufferedOutputStream(fo));
            out.setLevel(Deflater.BEST_COMPRESSION); // best compress

            for (File file : files) {
                FileInputStream fi = new FileInputStream(file);
                in = new BufferedInputStream(fi, 2048);
                String entryName = file.getName();
                ZipEntry entry = new ZipEntry(entryName);
                out.putNextEntry(entry);
                copyStream(in, out);
            }
            return true;

        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            return false;

        } finally {
            closeQuietly(out);
            closeQuietly(in);
        }
    }
}
