package tv.danmaku.android.log;

/**
 * Created by desmond on 1/11/17.
 */
/* package */ class NativeLogger implements ILogger {

    private LogSetting mSetting;

    public NativeLogger(LogSetting setting, boolean compress) {
        mSetting = setting;
        nativeSetup(setting.getLogDir(), InternalUtils.getProcessName(), compress);
    }

    public native void nativeSetup(String baseDir, String prefix, boolean compress);
    public native void nativeClose();
    public native void nativeFlush();
    public native void nativeLog(int priority, String process, String thread, String tag, String msg);
    public native void nativeEvent(String process, String thread, String tag, String msg);

    /* todo: file manager should write in native */
    public static native String getLogExt();
    public static native String getEventExt();

    @Override
    public void log(int priority, String tag, String msg) {
        if (mSetting.getLogfilePriority() == LogPriority.NONE || mSetting.getLogfilePriority() > priority) {
            return;
        }
        nativeLog(priority, InternalUtils.getProcessName(), Thread.currentThread().getName(), tag, msg);
    }

    @Override
    public void event(String tag, String msg) {
        nativeEvent(InternalUtils.getProcessName(), Thread.currentThread().getName(), tag, msg);
    }

    @Override
    public void flush() {
        nativeFlush();
    }

    @Override
    public void teardown() {
        nativeClose();
    }
}
