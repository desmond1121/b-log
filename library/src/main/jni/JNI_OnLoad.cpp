//
// Created by Jiayi Yao on 1/11/17.
//
#include <jni.h>
#include <string>
#include "mars_comm/scoped_jstring.h"
#include "includes/native_logger.h"

using namespace std;

unique_ptr<Logger> g_logger_ref = NULL;

void setup(JNIEnv *env, jclass /* this */, jstring _base_dir, jstring _prefix, jboolean _compress) {
    ASSERT(NULL != _base_dir);
    ScopedJstring _log_dir_jstr(env, _base_dir);

    if (NULL != _prefix) {
        ScopedJstring _prefix_jstr(env, _prefix);
        g_logger_ref = unique_ptr<Logger>(new Logger(_log_dir_jstr.GetChar(), _prefix_jstr.GetChar(), _compress));
    } else {
        g_logger_ref = unique_ptr<Logger>(new Logger(_log_dir_jstr.GetChar(), "", _compress));
    }
}

void teardown(JNIEnv *, jclass /* this */) {
    delete g_logger_ref.release();
}

void flush(JNIEnv *, jclass /* this */) {
    g_logger_ref->flush();
}

void log(JNIEnv *env, jclass /* this */,
         jint priority,
         jstring _proc,
         jstring _thread,
         jstring _tag,
         jstring _message) {
    const char* proc_cstr = NULL;
    const char* thread_cstr = NULL;
    const char* tag_cstr = NULL;
    const char* message_cstr = NULL;

    if (NULL != _tag) {
        tag_cstr = env->GetStringUTFChars(_tag, NULL);
    }

    if (NULL != _thread) {
        thread_cstr = env->GetStringUTFChars(_thread, NULL);
    }

    if (NULL != _proc) {
        proc_cstr = env->GetStringUTFChars(_proc, NULL);
    }

    if (NULL != _message) {
        message_cstr = env->GetStringUTFChars(_message, NULL);
    }

    if (priority == s_level_event) {
        g_logger_ref->logEvent(proc_cstr, thread_cstr, tag_cstr, message_cstr);
    } else {
        g_logger_ref->log(priority, proc_cstr, thread_cstr, tag_cstr, message_cstr);
    }

    if (NULL != _tag) {
        env->ReleaseStringUTFChars(_tag, tag_cstr);
    }

    if (NULL != _thread) {
        env->ReleaseStringUTFChars(_thread, thread_cstr);
    }

    if (NULL != _proc) {
        env->ReleaseStringUTFChars(_proc, proc_cstr);
    }

    if (NULL != _message) {
        env->ReleaseStringUTFChars(_message, message_cstr);
    }
}

void event(JNIEnv *env, jclass _thiz/* this */,
           jstring _proc,
           jstring _thread,
           jstring _tag,
           jstring _message) {
    log(env, _thiz, s_level_event, _proc, _thread, _tag, _message);
}

static jstring getLogExt(JNIEnv *env, jclass) {
    return env->NewStringUTF(s_ext_log);
}

static jstring getEventExt(JNIEnv *env, jclass) {
    return env->NewStringUTF(s_ext_event);
}

static const char *className = "tv/danmaku/android/log/NativeLogger";
static JNINativeMethod methods[] = {
        {"nativeSetup", "(Ljava/lang/String;Ljava/lang/String;Z)V", (void*) setup},
        {"nativeLog", "(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", (void*) log},
        {"nativeEvent", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", (void*) event},
        {"nativeFlush", "()V", (void*) flush},
        {"nativeClose", "()V", (void*) teardown},
        /* todo : use cpp */
        {"getLogExt", "()Ljava/lang/String;", (void*) getLogExt},
        {"getEventExt", "()Ljava/lang/String;", (void*) getEventExt},
};

jint JNI_OnLoad(JavaVM* vm, void*) {
    JNIEnv* env;
    if (vm->GetEnv((void **) &env, JNI_VERSION_1_6) != JNI_OK) {
        return JNI_ERR;
    }

    jclass clazz = env->FindClass(className);
    if (clazz == NULL) {
        return JNI_ERR;
    }
    if (env->RegisterNatives(clazz, methods,
                             sizeof(methods) / sizeof(methods[0])) < 0) {
        return JNI_ERR;
    }
    return JNI_VERSION_1_6;
}
