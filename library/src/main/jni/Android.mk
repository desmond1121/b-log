# Copyright (c) 2015-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the BSD-style license found in the
# LICENSE file in the root directory of this source tree. An additional grant
# of patent rights can be found in the PATENTS file in the same directory.

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := $(LOCAL_PATH)/includes

LOCAL_MODULE := blog
LOCAL_SRC_FILES := \
	JNI_OnLoad.cpp \
	native_logger.cpp \
	utils/appender.cpp \
	utils/assert.cpp \
	utils/autobuffer.cpp \
	utils/formatter.cpp \
	utils/log_buffer.cpp \
	utils/magica.cpp \
	utils/mmaper.cpp \
	utils/ptrbuffer.cpp \
	utils/scoped_jstring.cpp \

LOCAL_CFLAGS += -Wall -Werror -fexceptions -frtti
LOCAL_CFLAGS += -std=c++11

LOCAL_CPPFLAGS += -Wno-format -Wno-unused-parameter -Wno-unused-variable -Wno-missing-field-initializers

LOCAL_LDLIBS := -lz -llog

include $(BUILD_SHARED_LIBRARY)