//
// Created by Jiayi Yao on 1/11/17.
//

#pragma once

#include <string>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/mman.h>

class Mapper {

public:
    bool openMap(const char *_fpath, size_t _mapped_len);
    void closeMap();
    void syncMap();
    void* ptr();

private:
    bool _openMap(const char *, size_t , off_t);
    size_t length_;
    void* ptr_ = NULL;
};

