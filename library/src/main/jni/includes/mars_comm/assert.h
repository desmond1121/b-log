// Tencent is pleased to support the open source community by making Mars available.
// Copyright (C) 2016 THL A29 Limited, a Tencent company. All rights reserved.

// Licensed under the MIT License (the "License"); you may not use this file except in 
// compliance with the License. You may obtain a copy of the License at
// http://opensource.org/licenses/MIT

// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
// either express or implied. See the License for the specific language governing permissions and
// limitations under the License.


/*
 * assert.h
 *
 *  Created on: 2012-8-6
 *      Author: yerungui
 */
#pragma once

#include <sys/cdefs.h>
#include <stdarg.h>

#define ASSERT(e) ((e) ? (void) 0 : __ASSERT(__FILE__, __LINE__, __func__, #e))

namespace blog {
    void ENABLE_ASSERT();
    void DISABLE_ASSERT();
    int IS_ASSERT_ENABLE();
}

__attribute__((__nonnull__(1, 3, 4)))
void __ASSERT(const char*, int, const char*, const char*);
