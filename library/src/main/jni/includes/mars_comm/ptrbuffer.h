// Tencent is pleased to support the open source community by making Mars available.
// Copyright (C) 2016 THL A29 Limited, a Tencent company. All rights reserved.

// Licensed under the MIT License (the "License"); you may not use this file except in 
// compliance with the License. You may obtain a copy of the License at
// http://opensource.org/licenses/MIT

// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
// either express or implied. See the License for the specific language governing permissions and
// limitations under the License.
#pragma once

#include <sys/types.h>
#include <string>
#include "assert.h"

/**
 * Buffer with fixed length.
 *
 * Created by yerungui on 13-4-4.
 */
class PtrBuffer {
  public:
    enum TSeek {
        kSeekStart,
        kSeekCur,
    };
  public:
    PtrBuffer(void* _ptr, size_t _maxlen);
    PtrBuffer();
    ~PtrBuffer();

    template<class T> void Write(const T& _val)
    { Write(&_val, sizeof(_val)); }

    template<class T> void Write(int _nPos, const T& _val)
    { Write(&_val, sizeof(_val), _nPos);}

    void Write(const char* const _val)
    { Write(_val, (unsigned int)strlen(_val));}

    void Write(int _nPos, const char* const _val)
    { Write(_val, (unsigned int)strlen(_val), _nPos);}

    void Write(const void* _pBuffer, size_t _nLen);
    void Write(const void* _pBuffer, size_t _nLen, off_t _nPos);

    template<class T> void Read(T& _val)
    { Read(&_val, sizeof(_val)); }

    template<class T> void Read(int _nPos, const T& _val) const
    { Read(&_val, sizeof(_val), _nPos); }

    size_t Read(void* _pBuffer, size_t _nLen);
    size_t Read(void* _pBuffer, size_t _nLen, off_t _nPos) const;

    void Seek(size_t _nOffset,  TSeek _eOrigin = kSeekStart);

    void* Ptr();
    void* PosPtr();
    const void* Ptr() const;
    const void* PosPtr() const;

    size_t Length() const;
    size_t MaxLength() const;

    void Attach(void* _pBuffer, size_t _maxlen);
    void Reset();

  private:
    PtrBuffer(const PtrBuffer& _rhs);
    PtrBuffer& operator = (const PtrBuffer& _rhs);

  private:
    unsigned char* parray_;
    size_t cur_length_;
    size_t max_length_;
};
