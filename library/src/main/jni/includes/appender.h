//
// Created by Jiayi Yao on 1/15/17.
//

#pragma once

#include <string>
#include <thread>
#include <mutex>
#include <condition_variable>

#include "mars_comm/assert.h"
#include "log_buffer.h"
#include "mmaper.h"
#include "defs.h"

static const size_t s_buffer_block_length = 96 * 1024;
static const size_t s_max_single_log_length = 16 * 1024;

static const off_t s_max_log_file_size = 5 * 1024 * 1024; // 5mb

static const bool s_enable_mmap = true; // use false for debug non-mmap

/**
 * 首选mmap记录，不可用则使用内存缓存。
 */
class Appender {

public:
    Appender(const std::string& _baseDir,
             const std::string& _prefix,
             const std::string& _suffix,
             bool _isCompress);
    ~Appender();

    void append(const LogInfo* _info, const char* _data);
    void flush();
    void closeIfNeed();

private:
    void logToFile(void *_data, size_t _len);
    unsigned long getMappedHandle();
    void _appendAsync();
    void _writeToFile(const char* _fpath, void *_data, size_t _len);

    /* --- MULTI THREADS (Only for memory cache) --- */
    void _asyncLogThread();
    std::thread log_thread_;
    std::mutex mutex_buffer_;

    std::condition_variable condv_buffer_;
    bool terminate_;
    bool deconstructed_;
    std::mutex mutex_condv_;
    /* --- END MULTI THREADS --- */

    Mapper mapper_;
    std::unique_ptr<LogBuffer> buffer_;
    bool use_mmap_ = false;

    std::string base_dir_;
    std::string file_suffix_;
    std::string file_prefix_;
};