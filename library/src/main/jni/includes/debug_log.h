//
// Created by Jiayi Yao on 1/15/17.
//
#pragma once

//#define BLOG_DEBUG
#define LOG_TAG "BLOG_DEBUG"

#include <android/log.h>

#ifdef BLOG_DEBUG
#define __LOGD__(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#else
#define __LOGD__(...) void(0)
#endif

#define __LOGE__(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
