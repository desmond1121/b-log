//
// Created by Jiayi Yao on 2/6/17.
//

#pragma once

#include <stdlib.h>
#include <cstdint>
#include "debug_log.h"
#include "mars_comm/ptrbuffer.h"

static const char kMagicStart[2] = {'\x42', '\x4c'}; // BL
static const char kMagicEnd  = '\0';

class Magica {

public:
    int getHeaderLen();
    int getTailerLen();
    void setHeaderInfo(char* _data);
    void setTailerInfo(char* _data, uint32_t _len);
    uint32_t getLogLen(const char* _data);
    void updateLogLen(char* _data, uint32_t _len);
    bool fix(char *_data, uint32_t &_raw_log_len);

};