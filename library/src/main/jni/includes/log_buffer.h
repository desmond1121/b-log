//
// Created by Jiayi Yao on 1/15/17.
//
#pragma once

#include <zlib.h>
#include <string>
#include <stdint.h>
#include "debug_log.h"
#include "magica.h"
#include "mars_comm/ptrbuffer.h"
#include "mars_comm/autobuffer.h"

class LogBuffer {
public:
    LogBuffer(void* _pbuffer, size_t _len, bool _is_compress);
    ~LogBuffer();
    void attach(void *_pbuffer, size_t _len);
    void endStream();

public:
    PtrBuffer& data();

    void fixLength();
    void flush(AutoBuffer &_buff);
    bool write(const void *_data, size_t _length);

private:
    size_t _length();
    bool _reset();

private:
    bool is_compress_;

    Magica magica_;
    PtrBuffer buff_;
    z_stream zstream_;
};
