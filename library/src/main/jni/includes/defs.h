//
// Created by Jiayi Yao on 1/15/17.
//
#pragma once

#include <ctime>

struct LogInfo {
    int priority;
    time_t time;
    const char* tag;
    const char* thread;
    const char* process;
};

static const int s_level_event = -1;
static const char* s_ext_log = "log";
static const char* s_ext_event = "event";
