//
// Created by Jiayi Yao on 1/15/17.
//

#pragma once

#include <string>
#include "android/log.h"
#include "mars_comm/ptrbuffer.h"
#include "defs.h"

void formatLog(const LogInfo *_info, const char *_data, PtrBuffer &_log);
const char* getPriorityString(int priority);

