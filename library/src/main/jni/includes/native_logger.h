//
// Created by Jiayi Yao on 1/15/17.
//

#pragma once

#include <memory>
#include <ctime>
#include "appender.h"

class Logger {

public:
    Logger(const std::string& _baseDir, const std::string& _prefix, bool _isCompress);
    ~Logger();
    void log(int _prior, const char *_proc, const char *_thread, const char *_tag, const char *_data);
    void logEvent(const char *_proc, const char *_thread, const char *_tag, const char *_data);
    void flush();

private:
    std::unique_ptr<Appender> log_appender_;
    std::unique_ptr<Appender> event_appender_;
};

