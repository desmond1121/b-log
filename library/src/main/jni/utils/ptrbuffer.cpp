// Tencent is pleased to support the open source community by making Mars available.
// Copyright (C) 2016 THL A29 Limited, a Tencent company. All rights reserved.

// Licensed under the MIT License (the "License"); you may not use this file except in 
// compliance with the License. You may obtain a copy of the License at
// http://opensource.org/licenses/MIT

// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
// either express or implied. See the License for the specific language governing permissions and
// limitations under the License.

#include "mars_comm/ptrbuffer.h"

/**
 * Buffer with fixed length.
 *
 * Created by yerungui on 13-4-4.
 *
 * Modified by desmond.
 */

#ifndef max
#define max(a, b) (((a) > (b)) ? (a) : (b))
#endif
#ifndef min
#define min(a, b) (((a) < (b)) ? (a) : (b))
#endif

PtrBuffer::PtrBuffer(void* _ptr, size_t _maxlen)
    : parray_((unsigned char*)_ptr)
    , cur_length_(0)
    , max_length_(_maxlen) {
}

PtrBuffer::PtrBuffer() {
    Reset();
}

PtrBuffer::~PtrBuffer() {
}

void PtrBuffer::Write(const void* _pBuffer, size_t _nLen) {
    Write(_pBuffer, _nLen, Length());
    Seek(_nLen, kSeekCur);
}

void PtrBuffer::Write(const void* _pBuffer, size_t _nLen, off_t _nPos) {
    ASSERT(NULL != _pBuffer);
    ASSERT(0 <= _nPos);
    size_t _copy_len = min(_nLen, max_length_ - _nPos);
    memcpy((unsigned char*)Ptr() + _nPos, _pBuffer, _copy_len);
}

size_t PtrBuffer::Read(void* _pBuffer, size_t _nLen) {
    size_t nRead = Read(_pBuffer, _nLen, Length());
    Seek(nRead, kSeekCur);
    return nRead;
}

size_t PtrBuffer::Read(void* _pBuffer, size_t _nLen, off_t _nPos) const {
    ASSERT(NULL != _pBuffer);
    ASSERT(0 <= _nPos);

    size_t _read_len = min(_nLen, max_length_ - _nPos);
    memcpy(_pBuffer, PosPtr(), _read_len);
    return _read_len;
}

void  PtrBuffer::Seek(size_t _nOffset, TSeek _eOrigin) {
    switch (_eOrigin) {
    case kSeekStart:
        cur_length_ = _nOffset;
        break;

    case kSeekCur:
        cur_length_ += _nOffset;
        break;

    default:
        ASSERT(false);
        break;
    }

    if ((unsigned int)cur_length_ > max_length_) {
        cur_length_ = max_length_;
    }
}

void*  PtrBuffer::Ptr() {
    return parray_;
}

const void*  PtrBuffer::Ptr() const {
    return parray_;
}

void* PtrBuffer::PosPtr() {
    return ((unsigned char*)Ptr()) + Length();
}

const void* PtrBuffer::PosPtr() const {
    return ((unsigned char*)Ptr()) + Length();
}

size_t PtrBuffer::Length() const {
    return cur_length_;
}

size_t PtrBuffer::MaxLength() const {
    return max_length_;
}

void PtrBuffer::Attach(void* _pBuffer, size_t _maxlen) {
    Reset();
    parray_ = (unsigned char*)_pBuffer;
    cur_length_ = 0;
    max_length_ = _maxlen;
}

void PtrBuffer::Reset() {
    parray_ = NULL;
    cur_length_ = 0;
    max_length_ = 0;
}

