//
// Created by Jiayi Yao on 2/6/17.
//
#include "magica.h"


static bool isMatchMagic(const char* _data) {
    return _data[0] == kMagicStart[0] && _data[1] == kMagicStart[1];
}

/*
 * |magic start(char)|length(uint32_t)|
 */
int Magica::getHeaderLen() {
    return sizeof(char) * 2 + sizeof(uint32_t);
}

int Magica::getTailerLen() {
    return sizeof(kMagicEnd);
}

void Magica::setHeaderInfo(char* _data) {
    __LOGD__("Add header info to %lu", reinterpret_cast<long>(_data));
    memcpy(_data, kMagicStart, sizeof(char) * 2);


    uint32_t len = 0;
    memcpy(_data + sizeof(char) * 2, &len, sizeof(uint32_t));
}

void Magica::setTailerInfo(char* _data, uint32_t _len) {
    memcpy(_data + _len + 1, &kMagicEnd, sizeof(kMagicEnd));
}

uint32_t Magica::getLogLen(const char* _data) {
    if (!isMatchMagic(_data)) {
        return 0;
    }

    uint32_t len = 0;
    memcpy(&len, _data + getHeaderLen() - sizeof(uint32_t), sizeof(uint32_t));
    return len;
}

void Magica::updateLogLen(char* _data, uint32_t _len) {
    uint32_t _before_len = getLogLen(_data);
    uint32_t _after_len = _before_len + _len;
    memcpy(_data + getHeaderLen() - sizeof(uint32_t), &_after_len, sizeof(_after_len));
}

bool Magica::fix(char *_data, uint32_t &_raw_log_len) {
    if (!isMatchMagic(_data)) {
        return false;
    }

    _raw_log_len = getLogLen(_data);
    return true;
}
