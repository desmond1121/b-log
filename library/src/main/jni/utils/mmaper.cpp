//
// Created by Jiayi Yao on 1/11/17.
//
#include "mmaper.h"
#include "debug_log.h"

bool Mapper::openMap(const char *_fpath, size_t _mapped_len) {
    if (NULL != ptr_) {
        closeMap();
    }

    return _openMap(_fpath, _mapped_len, 0);
}

bool Mapper::_openMap(const char *_fpath, size_t _len, off_t _offset) {
    int fd = open(_fpath, O_RDWR | O_CREAT);
    if (fd < 0) {
        __LOGE__("Cannot get mmap fd.");
        return false;
    }

    off_t _need = static_cast<off_t>(_offset + _len);
    ftruncate(fd, _need);

    void* _ptr = mmap(NULL, _len, PROT_READ | PROT_WRITE, MAP_SHARED, fd, _offset);
    close(fd);
    if (_ptr == MAP_FAILED) {
        __LOGE__("Map failed! Error(%d): %s", errno, strerror(errno));
        return false;
    }

    ptr_ = _ptr;
    return true;
}

void Mapper::syncMap() {
    msync(ptr_, length_, MS_SYNC);
}

void Mapper::closeMap() {
    munmap(ptr_, length_);
    ptr_ = NULL;
}

void *Mapper::ptr() {
    return ptr_;
}


























