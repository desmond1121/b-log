//
// Created by Jiayi Yao on 1/15/17.
//

#include "log_buffer.h"

LogBuffer::LogBuffer(void* _pbuffer, size_t _len, bool _is_compress) : is_compress_(_is_compress) {
    attach(_pbuffer, _len);
}

LogBuffer::~LogBuffer() {
    endStream();
}

void LogBuffer::endStream() {
    if (Z_NULL != zstream_.state) {
        __LOGD__("deflateEnd");
        deflateEnd(&zstream_);
    }
}

PtrBuffer& LogBuffer::data() {
    return buff_;
}

size_t LogBuffer::_length(){
    if (is_compress_) {
        return magica_.getLogLen((const char *) buff_.Ptr());
    }
    return buff_.Length();
}

void LogBuffer::flush(AutoBuffer &_buff) {
    endStream();

    if (_length() > 0){
        if (is_compress_) {
            magica_.setTailerInfo((char *) buff_.Ptr(), (uint32_t) buff_.Length());
            buff_.Seek(buff_.Length() + magica_.getTailerLen());
        }
        _buff.Write(buff_.Ptr(), buff_.Length());
    }

    memset(buff_.Ptr(), 0, buff_.MaxLength());
    buff_.Seek(0);
}


bool LogBuffer::write(const void *_data, size_t _length) {
    if (NULL == _data || 0 == _length) {
        return false;
    }

    if (buff_.Length() == 0) {
        if (!_reset()) {
            __LOGE__("Reset err!");
            return false;
        }

        if (is_compress_) {
            magica_.setHeaderInfo((char *) buff_.Ptr());
            buff_.Seek((size_t) magica_.getHeaderLen());
        }
    }

    size_t _before_len = buff_.Length();
    if (is_compress_) {
        zstream_.avail_in = (uInt)_length;
        zstream_.next_in = (Bytef*)_data;

        uInt avail_out = (uInt)(buff_.MaxLength() - buff_.Length());
        zstream_.next_out = (Bytef*)buff_.PosPtr();
        zstream_.avail_out = avail_out;

        int result;
        if ((result = deflate(&zstream_, Z_SYNC_FLUSH)) != Z_OK) {
            __LOGE__("Deflate error : %s!", zError(result));
            return false;
        }
        size_t _write_len = avail_out - zstream_.avail_out;
        buff_.Seek(_write_len, PtrBuffer::kSeekCur);
        size_t _write = buff_.Length() - _before_len;
        magica_.updateLogLen((char *) buff_.Ptr(), static_cast<uint32_t>(_write));
    } else {
        buff_.Write(_data, _length);
    }

    return true;
}

bool LogBuffer::_reset() {
    if (is_compress_) {
        if (Z_NULL != zstream_.state) {
            deflateEnd(&zstream_);
        }

        __LOGD__("Reset zlib buffer!");
        zstream_.zalloc = Z_NULL;
        zstream_.zfree = Z_NULL;
        zstream_.opaque = Z_NULL;
        
        if (Z_OK != deflateInit2(&zstream_, Z_BEST_COMPRESSION, Z_DEFLATED, -MAX_WBITS, MAX_MEM_LEVEL, Z_DEFAULT_STRATEGY)) {
            return false;
        }
    }
    return true;
}

void LogBuffer::fixLength() {
    if (is_compress_) {
        uint32_t len;
        magica_.fix((char *) buff_.Ptr(), len);
        buff_.Seek(len + magica_.getHeaderLen());
    } else { // find first '\0'
        char* _data = new char[buff_.MaxLength()];
        strncpy(_data, (const char *) buff_.Ptr(), buff_.MaxLength() - 1);
        _data[buff_.MaxLength() - 1] = 0;
        size_t _len = strlen(_data);
        buff_.Seek(_len, PtrBuffer::kSeekStart);
        delete[] _data;
    }
}

void LogBuffer::attach(void *_pbuffer, size_t _len) {
    buff_.Attach(_pbuffer, _len);
    memset(&zstream_, 0, sizeof(zstream_));
}



