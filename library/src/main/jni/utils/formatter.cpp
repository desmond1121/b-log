//
// Created by Jiayi Yao on 1/15/17.
//
#include <debug_log.h>
#include "formatter.h"

using namespace std;

void formatLog(const LogInfo *_info, const char *_data, PtrBuffer &_log) {
    if (NULL != _info) {
        char temp_time[64];

        if (0 != _info->time) {
            time_t _time = _info->time;
            tm tm = *localtime((const time_t *) &_time);
            snprintf(temp_time, sizeof(temp_time), "%d-%02d-%02d %02d:%02d:%02d",
                     1900 + tm.tm_year,
                     1 + tm.tm_mon,
                     tm.tm_mday,
                     tm.tm_hour,
                     tm.tm_min,
                     tm.tm_sec);
        }
        int ret = snprintf((char *) _log.PosPtr(), 1024,
                           "[%s] P/%s T/%s %s/%s: ",
                           temp_time,
                           _info->process,
                           _info->thread,
                           getPriorityString(_info->priority),
                           _info->tag ? _info->tag : "NONE");

        ASSERT(0 <= ret);
        _log.Seek((size_t) ret, PtrBuffer::kSeekCur);
    }

    if (NULL != _data) {
        // in android 64bit, in strnlen memchr,  const unsigned char*  end = p + n;  > 4G!!!!! in stack array

        size_t bodylen =
                _log.MaxLength() - _log.Length() > 130 ? _log.MaxLength() - _log.Length() - 130 : 0;
        bodylen = bodylen > 0xFFFFU ? 0xFFFFU : bodylen;
        bodylen = strnlen(_data, bodylen);
        bodylen = bodylen > 0xFFFFU ? 0xFFFFU : bodylen;
        _log.Write(_data, bodylen);
    } else {
        _log.Write("NOTHING");
    }

    char nextline = '\n';

    if (*((char *) _log.PosPtr() - 1) != nextline) {
        _log.Write(&nextline, 1);
    }
}

const char *getPriorityString(int priority) {
    switch (priority) {
        case ANDROID_LOG_UNKNOWN:
            return "UNKNOWN";
        case ANDROID_LOG_DEFAULT:
            return "DEFAULT";
        case ANDROID_LOG_VERBOSE:
            return "V";
        case ANDROID_LOG_DEBUG:
            return "D";
        case ANDROID_LOG_INFO:
            return "I";
        case ANDROID_LOG_WARN:
            return "W";
        case ANDROID_LOG_ERROR:
            return "E";
        case ANDROID_LOG_FATAL:
            return "F";
        default:
            return "EVENT";
    }
}













