//
// Created by Jiayi Yao on 1/15/17.
//
#include "formatter.h"
#include "appender.h"

using namespace std;

/**
 * 首选mmap，不可用则使用内存记录。
 */
Appender::Appender(const std::string& _baseDir,
                   const std::string& _prefix,
                   const std::string& _suffix,
                   bool _isCompress) {
    __LOGD__("[CONSTRUCT] appender(%lu)", reinterpret_cast<unsigned long>(this));
    base_dir_ = _baseDir;

    struct stat _sb;
    if (stat(_baseDir.c_str(), &_sb) == -1) {
        mkdir(_baseDir.c_str(), _sb.st_mode);
    }

    file_prefix_ = _prefix;
    file_suffix_ = _suffix;

    string _cache = base_dir_ + "/" + file_prefix_ + "-cache." + file_suffix_ + ".mmap";
    __LOGD__("Open log at %s, prefix: %s", _baseDir.c_str(), file_prefix_.c_str());

    if (s_enable_mmap && mapper_.openMap(_cache.c_str(), s_buffer_block_length)) {
        buffer_ = unique_ptr<LogBuffer>(new LogBuffer(mapper_.ptr(), s_buffer_block_length, _isCompress));
        buffer_->fixLength();
        use_mmap_ = true;
        __LOGD__("Use mmap cache (handle: %lu), data usage (%ld/%ld)",
                 getMappedHandle(),
                 buffer_->data().Length(),
                 buffer_->data().MaxLength());
    } else { // mmap fail
        char* buffer = new char[s_buffer_block_length];
        buffer_ = unique_ptr<LogBuffer>(new LogBuffer(buffer, s_buffer_block_length, _isCompress));
        __LOGD__("Use memory cache.");
    }

    if (buffer_->data().Length() > 0) {
        flush();
    }

    append(NULL, "___BEGIN___LOG___");
    terminate_ = false;
    deconstructed_ = false;
    log_thread_ = thread(&Appender::_asyncLogThread, this);
}

Appender::~Appender() {
    __LOGD__("[DECONSTRUCT] appender(%lu)", reinterpret_cast<unsigned long>(this));
    /* deconstructed appender not flush data. */
    deconstructed_ = true;
    closeIfNeed();
}

void Appender::closeIfNeed() {
    if (NULL == buffer_) {
        return;
    }

    terminate_ = true;
    condv_buffer_.notify_all();
    if (log_thread_.joinable()) {
        log_thread_.join();
    }

    if (use_mmap_) {
        mapper_.closeMap();
    } else {
        delete[] (char*)((buffer_->data()).Ptr());
    }

    buffer_->endStream();
    delete buffer_.release();
}

void Appender::append(const LogInfo* _info, const char* _data) {
    lock_guard<mutex> lock_buffer(mutex_buffer_);

    char _tmp_char[s_max_single_log_length] = {0};
    PtrBuffer _tmp_buff(_tmp_char, s_max_single_log_length);

    formatLog(_info, _data, _tmp_buff);

//    __LOGD__("Log data (%ld/%ld): %s", buffer_->data().Length(), s_buffer_block_length, _tmp_char);

    if (!buffer_->write(_tmp_buff.Ptr(), static_cast<unsigned int>(_tmp_buff.Length()))) {
        return;
    }

    _appendAsync();
}

void Appender::_appendAsync() {
    if (buffer_->data().Length() >= buffer_->data().MaxLength() * 3 / 5) {
        condv_buffer_.notify_all();
    }
}

void Appender::flush() {
    unique_lock<mutex> lock_buffer(mutex_buffer_);
    AutoBuffer _tmp;
    buffer_->flush(_tmp);
    lock_buffer.unlock();

    if (NULL != _tmp.Ptr()) {
        logToFile(_tmp.Ptr(), _tmp.Length());
    }
}

void Appender::_asyncLogThread() {
    while (!terminate_) {
        // wait for notify
        unique_lock<mutex> lk(mutex_condv_);
        condv_buffer_.wait(lk);

        // deconstructed appender would not flush!
        if (deconstructed_ || NULL == buffer_) {
            break;
        }

        flush();
    }
}

/**
 * 修改此方法要慎重，因为Java层使用相同规范来寻找方法
 */
void _makeLogFile(char *_filepath, string _basedir,
                  string _prefix, string _suffix, unsigned int _len) {
    time_t _tmp_time;
    time(&_tmp_time);

    tm tm = *localtime((const time_t *) &_tmp_time);

    std::string _tmp_str = _basedir;
    _tmp_str += "/";
    char temp [256] = {0};
    snprintf(temp, 256, "%04d%02d%02d",
             1900 + tm.tm_year, 1 + tm.tm_mon, tm.tm_mday);
    _tmp_str += temp;
    _tmp_str += "-" + _prefix + "." + _suffix;

    strncpy(_filepath, _tmp_str.c_str(), _len - 1);
    _filepath[_len - 1] = '\0';
}

void Appender::_writeToFile(const char* _fpath, void *_data, size_t _len) {
    if (_fpath == NULL) {
        ASSERT(false);
    }

    FILE* _target = fopen(_fpath, "ab+");

    long _before_len = ftell(_target);


    if (_before_len < 0) {
        ASSERT(false);
    }

    if (1 != fwrite(_data, _len, 1, _target)) {
        __LOGE__("Error happen!");

        ftruncate(fileno(_target), _before_len);
        fseek(_target, 0, SEEK_END);

        int fd = open(_fpath, O_RDWR);
        ssize_t result = write(fd, "test", 4);
        __LOGE__("Simple write result: %ld", result);

        char err_log[256] = {0};
        snprintf(err_log, sizeof(err_log), "Write file error: %s\n", strerror(errno));
        __LOGE__(err_log);
        close(fd);
        return;
    }

    long _after_len = ftell(_target);
    fclose(_target);

    __LOGD__("Write %ld byte data to file (%ld byte) %s", _len, _after_len, _fpath);

    if (_after_len >= s_max_log_file_size) {
        char _prev_fp[512] = {0};
        // make file to 20170505-main-prev.log
        _makeLogFile(_prev_fp, base_dir_, file_prefix_ + "_prev", file_suffix_, 512);

        rename(_fpath, _prev_fp);
        __LOGD__("File exceed length! Rename %s to %s.", _fpath, _prev_fp);
    }
}

void Appender::logToFile(void *_data, size_t _len) {
    if (NULL == _data || 0 == _len) {
        return;
    }

    char _tmp[512] = {0};
    _makeLogFile(_tmp, base_dir_, file_prefix_, file_suffix_, 512);

    _writeToFile(_tmp, _data, _len);
}

unsigned long Appender::getMappedHandle() {
    return reinterpret_cast<unsigned long>(mapper_.ptr());
}







