#include <signal.h>

#include "mars_comm/assert.h"
#include "debug_log.h"

#ifdef DEBUG_BLOG
static int sg_enable_assert = 1;
#else
static int sg_enable_assert = 0;
#endif

void blog::ENABLE_ASSERT() {
    sg_enable_assert = 1;
}

void blog::DISABLE_ASSERT() {
    sg_enable_assert = 0;
}

int blog::IS_ASSERT_ENABLE() {
    return sg_enable_assert;
}

void __assert2(const char * _pfile, int _line, const char * _pfunc, const char * _pexpression) {
    __LOGE__("Assert (%s) error at file \"%s\", line %d, function %s", _pexpression, _pfile, _line, _pfunc);
}

void __ASSERT(const char * _pfile, int _line, const char * _pfunc, const char * _pexpression) {
    if (blog::IS_ASSERT_ENABLE()) {
        raise(SIGTRAP);
        __assert2(_pfile, _line, _pfunc, _pexpression);
    }
}
