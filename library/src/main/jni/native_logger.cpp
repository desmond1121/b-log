//
// Created by Jiayi Yao on 1/15/17.
//

#include "native_logger.h"

using namespace std;

Logger::Logger(const string& _baseDir,
               const string& _prefix,
               bool _isCompress) {
    __LOGD__("[CONSTRUCT] logger(%lu)", reinterpret_cast<unsigned long>(this));
    ASSERT(!_baseDir.empty());
    string prefix = _prefix.empty() ? "blog" : _prefix;

    log_appender_ = unique_ptr<Appender>(new Appender(_baseDir, prefix, s_ext_log, _isCompress));
    event_appender_ = unique_ptr<Appender>(new Appender(_baseDir, prefix, s_ext_event, _isCompress));
}

Logger::~Logger() {
    __LOGD__("[DECONSTRUCT] logger(%lu)", reinterpret_cast<unsigned long>(this));
}

void Logger::logEvent(const char *_proc, const char *_thread, const char *_tag, const char *_data) {
    unique_ptr<LogInfo> info_ptr(new LogInfo);
    info_ptr->priority = s_level_event;
    time(&(info_ptr->time));
    info_ptr->process = _proc;
    info_ptr->thread = _thread;
    info_ptr->tag = _tag;
    event_appender_->append(info_ptr.get(), _data);
}

void Logger::log(int _prior, const char *_proc, const char *_thread, const char *_tag, const char *_data) {
    unique_ptr<LogInfo> info_ptr(new LogInfo);
    info_ptr->priority = _prior;
    time(&(info_ptr->time));
    info_ptr->process = _proc;
    info_ptr->thread = _thread;
    info_ptr->tag = _tag;
    log_appender_->append(info_ptr.get(), _data);
}

void Logger::flush() {
    log_appender_->flush();
    event_appender_->flush();
}
