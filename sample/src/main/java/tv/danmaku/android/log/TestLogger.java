package tv.danmaku.android.log;

import android.os.Handler;
import android.os.Looper;

import tv.danmaku.android.log.BLog;

/**
 * Created by desmond on 1/20/17.
 */
public class TestLogger {

    private boolean isStop = false;
    private ILogger logger;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            while (!isStop) {
                doLog();
            }
        }
    };

    private void doLog() {
        logger.log(LogPriority.ERROR, "续一秒", "谈笑风生/一点微小的工作/教你做人/成天就想搞个大新闻/无可奉告/身经百战/续一秒/谈笑风生/一点微小的工作/教你做人/成天就想搞个大新闻/无可奉告/身经百战/续一秒/谈笑风生/一点微小的工作/教你做人/成天就想搞个大新闻/无可奉告/身经百战/续一秒/谈笑风生/一点微小的工作/教你做人/成天就想搞个大新闻/无可奉告/身经百战/续一秒/谈笑风生/一点微小的工作/教你做人/成天就想搞个大新闻/无可奉告/身经百战/续一秒");
    }

    void start(Looper looper, ILogger logger) {
        isStop = false;
        this.logger = logger;
        Handler handler = new Handler(looper);
        handler.post(runnable);
    }

    void stop() {
        isStop = true;
    }
}
