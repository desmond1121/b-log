/*
 * Copyright (c) 2016. bilibili
 */

package tv.danmaku.android.log;

import android.support.annotation.WorkerThread;

/**
 * Old b-log code, only use for comparison.
 */
@Deprecated
class LogFileSyncImpl implements ILogger {

    private final String mFilePath;
    private final LogSetting mSetting;
    private final Files mFiles;

    public LogFileSyncImpl(LogSetting setting) {
        mSetting = setting;
        mFiles = Files.instance(setting);
        mFilePath = mFiles.getLogPath();
    }

    @WorkerThread
    public void log(int priority, String tag, String msg) {
        if (mSetting.getLogfilePriority() == LogPriority.NONE
                || mSetting.getLogfilePriority() > priority) {
            return;
        }

        // get logMessage from Object Pools
        Files.LogMessage logMessage = Files.LogMessage.obtain();
        logMessage.setMessage(priority, System.currentTimeMillis(), tag,
                Thread.currentThread().getName(), msg);

        if (mFiles.canWrite(mFilePath)) {
            mFiles.writeToFile(logMessage, mFilePath);
        }
    }

    @Override
    public void event(String tag, String msg) {

    }

    @Override
    public void flush() {

    }

    @Override
    public void teardown() {

    }
}
