package tv.danmaku.android.log;

import android.content.Intent;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.util.List;

import tv.danmaku.android.R;

/**
 * Created by desmond on 1/11/17.
 */
public class MainActivity extends AppCompatActivity implements Handler.Callback{
    private static final String TAG = "BLOG_DEBUG_TEST";

    static {
        System.loadLibrary("gnustl_shared");
        System.loadLibrary("blog");
    }

    private TestLogger logger = new TestLogger();
    private NativeLogger nativeLogger;
    private LogFileImpl fileLogger;
    private FileManager fileManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initLogger();
        final HandlerThread thread = new HandlerThread("thread_blog_sample_io");
        thread.start();
        final Handler handler = new Handler(thread.getLooper());

        findViewById(R.id.start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.util.Log.d(TAG, "Start b-log at main process!");
                logger.start(thread.getLooper(), nativeLogger);
                findViewById(R.id.start).setClickable(false);
                findViewById(R.id.end).setClickable(true);
            }
        });

        findViewById(R.id.end).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.util.Log.d(TAG, "Stop main process b-log!");
                logger.stop();
                findViewById(R.id.start).setClickable(true);
                findViewById(R.id.end).setClickable(false);
            }
        });

        findViewById(R.id.start_service).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.util.Log.d(TAG, "Start b-log at process service!");
                startService(new Intent(MainActivity.this, LogTestService.class));
                findViewById(R.id.start_service).setClickable(false);
                findViewById(R.id.stop_service).setClickable(true);
            }
        });

        findViewById(R.id.stop_service).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.util.Log.d(TAG, "Stop service process b-log!");
                stopService(new Intent(MainActivity.this, LogTestService.class));
                findViewById(R.id.start_service).setClickable(true);
                findViewById(R.id.stop_service).setClickable(false);
            }
        });

        findViewById(R.id.test_blog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        testLogger(fileLogger);
                    }
                });
            }
        });

        findViewById(R.id.test_blog_v2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        testLogger(nativeLogger);
                    }
                });
            }
        });

        findViewById(R.id.test_file).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File[] files = fileManager.queryFiles(LogSetting.LOG, System.currentTimeMillis());

                if (files == null) {
                    android.util.Log.d(TAG, "NULL!");
                } else {
                    for (File file : files) {
                        android.util.Log.d(TAG, file.getAbsolutePath());
                    }
                }
            }
        });

        findViewById(R.id.zip_file).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File file = fileManager.zippingFiles(LogSetting.LOG, System.currentTimeMillis(), null);

                if (file == null) {
                    android.util.Log.d(TAG, "NULL!");
                } else {
                    android.util.Log.d(TAG, file.getAbsolutePath());
                }
            }
        });

        findViewById(R.id.clear_file).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> cleaned = fileManager.cleanExpiredLogs();

                if (cleaned == null) {
                    android.util.Log.d(TAG, "Clean nothing!");
                } else {
                    for (String str : cleaned) {
                        android.util.Log.d(TAG, "Clean " + str);
                    }
                }
            }
        });
    }

    private void initLogger() {
        LogSetting logSetting = new LogSetting.Builder(MainActivity.this)
                .setLogDir(MainActivity.this.getExternalFilesDir("origin-log").getAbsolutePath())
                .setDebuggable(true).build();
        fileLogger = new LogFileImpl(logSetting);

        logSetting = new LogSetting.Builder(MainActivity.this)
                .setLogDir(MainActivity.this.getExternalFilesDir("native-log").getAbsolutePath())
                .setDebuggable(true).build();
        nativeLogger = new NativeLogger(logSetting, true);
        fileManager = new FileManager(logSetting);
    }

    long start = -1;

    void testLogger(ILogger logger) {
        long nativeHeapSize = Debug.getNativeHeapAllocatedSize();
        long javaHeapSize = getAllocatedMemory();
        begin();
        for (int i = 0; i < 100; i++) {
            logger.log(LogPriority.ERROR, "续一秒", "谈笑风生/一点微小的工作/教你做人/成天就想搞个大新闻/无可奉告/身经百战/续一秒/谈笑风生/一点微小的工作/教你做人/成天就想搞个大新闻/无可奉告/身经百战/续一秒/谈笑风生/一点微小的工作/教你做人/成天就想搞个大新闻/无可奉告/身经百战/续一秒/谈笑风生/一点微小的工作/教你做人/成天就想搞个大新闻/无可奉告/身经百战/续一秒/谈笑风生/一点微小的工作/教你做人/成天就想搞个大新闻/无可奉告/身经百战/续一秒");
        }
        end(logger.getClass().getName());
        Log.d(TAG, "Native heap change: " + (Debug.getNativeHeapAllocatedSize() - nativeHeapSize) / 1024
                + "\nJava heap change:" + (getAllocatedMemory() - javaHeapSize) / 1024);
    }

    void begin() {
        start = System.currentTimeMillis();
    }

    void end(String name) {
        long t = System.currentTimeMillis();
        Log.d(TAG, name + " use time " + (t - start) + "ms");
    }

    long getAllocatedMemory() {
        return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    }

    @Override
    public boolean handleMessage(Message msg) {
        return false;
    }
}
