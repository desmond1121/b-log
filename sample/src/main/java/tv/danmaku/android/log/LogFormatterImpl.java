/*
 * Copyright (c) 2016. bilibili
 */

package tv.danmaku.android.log;

import java.text.SimpleDateFormat;
import java.util.Locale;


/**
 * Old b-log code, only use for comparison.
 */
@Deprecated
class LogFormatterImpl implements LogFormatter {

    private final SimpleDateFormat mDateFormatter;

    public LogFormatterImpl() {
        mDateFormatter = new SimpleDateFormat("MM-dd HH:mm:ss.SSS", Locale.getDefault());
    }

    @Override
    public String emptyMessage() {
        return "Empty/NULL";
    }

    @Override
    public String buildMessage(int priority, long time, String tag, String thread, String msg) {
        StringBuilder sb = new StringBuilder();
        sb.append(mDateFormatter.format(time))
                .append("  ")
                .append(LogPriority.getName(priority))
                .append("/")
                .append(tag)
                .append("  ");

        sb.append("[")
                .append(thread)
                .append("]  ")
                .append(msg);

        return sb.toString();
    }
}
