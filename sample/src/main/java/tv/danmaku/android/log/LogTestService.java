package tv.danmaku.android.log;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by desmond on 1/20/17.
 */
public class LogTestService extends Service {

    NativeLogger logger;

    @Override
    public void onCreate() {
        super.onCreate();
        LogSetting logSetting = new LogSetting.Builder(LogTestService.this)
                .setLogDir(LogTestService.this.getExternalFilesDir("native-log").getAbsolutePath())
                .setDebuggable(true).build();
        logger = new NativeLogger(logSetting, false);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        for (int i = 0; i < 10000000; i++) {
            logger.log(LogPriority.ERROR, "续一秒", "谈笑风生/一点微小的工作/教你做人/成天就想搞个大新闻/无可奉告/身经百战/续一秒/谈笑风生/一点微小的工作/教你做人/成天就想搞个大新闻/无可奉告/身经百战/续一秒/谈笑风生/一点微小的工作/教你做人/成天就想搞个大新闻/无可奉告/身经百战/续一秒/谈笑风生/一点微小的工作/教你做人/成天就想搞个大新闻/无可奉告/身经百战/续一秒/谈笑风生/一点微小的工作/教你做人/成天就想搞个大新闻/无可奉告/身经百战/续一秒");
        }
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
