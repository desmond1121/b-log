/*
 * Copyright (c) 2016. bilibili
 */

package tv.danmaku.android.log;

import android.support.annotation.WorkerThread;

import java.util.LinkedList;
import java.util.List;

/**
 * Old b-log code, only use for comparison.
 */
@Deprecated
class LogFileImpl implements ILogger {

    private static final int LOG_TASK_ID = 0x222;

    private final String mFilePath;
    private final byte[] mLock = new byte[0];
    private final LogSetting mSetting;
    private final Files mFiles;
    private final List<Files.LogMessage> mCacheQueue;

    private final Runnable mWriteTask = new Runnable() {
        @Override
        public void run() {
            writeToFile();
        }
    };

    public LogFileImpl(LogSetting setting) {
        mSetting = setting;
        mFiles = Files.instance(setting);
        mCacheQueue = new LinkedList<>();
        mFilePath = mFiles.getLogPath();
    }

    @Override
    public void log(int priority, String tag, String msg) {
        if (mSetting.getLogfilePriority() == LogPriority.NONE
                || mSetting.getLogfilePriority() > priority) {
            return;
        }

        // get logMessage from Object Pools
        Files.LogMessage logMessage = Files.LogMessage.obtain();
        logMessage.setMessage(priority, System.currentTimeMillis(), tag, Thread.currentThread().getName(), msg);

        // add to list
        synchronized (mLock) {
            mCacheQueue.add(logMessage);
            if (mCacheQueue.size() > 20) {
                Executor.instance().removeMessage(mWriteTask);
                Executor.instance().post(mWriteTask);
            }
        }

        // write to file
        if (!Executor.instance().hasMessages(LOG_TASK_ID)) {
            Executor.instance().postMessage(LOG_TASK_ID, mWriteTask);
        }
    }

    @Override
    public void event(String tag, String msg) {

    }

    @Override
    public void flush() {}

    @Override
    public void teardown() {}

    @WorkerThread
    private void writeToFile() {
        if (mFiles.canWrite(mFilePath)) {
            List<Files.LogMessage> list;

            synchronized (mLock) {
                list = new LinkedList<>(mCacheQueue);
                mCacheQueue.clear();
            }

            mFiles.writeToFile(list, mFilePath);
        }
    }

}
