/*
 * Copyright (c) 2016. bilibili
 */

package tv.danmaku.android.log;


/**
 * Old b-log code, only use for comparison.
 */
@Deprecated
public interface LogFormatter {

    /**
     * Get empty log message style.
     */
    String emptyMessage();

    /**
     * Build a log message to log in file.
     */
    String buildMessage(int priority, long time, String tag, String thread, String msg);
}
