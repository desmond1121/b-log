#!/usr/bin/python

# Use this file to decode b-log file.
#
# usage:
# $ python decode_script.py 20170204-main.log 20170204-game.log
# Decode 20170204-main.log to 20170204-main.log.txt
# Decode 20170204-game.log to 20170204-game.log.txt
#
# @author desmond

import sys
import os
import glob
import zlib
import struct

MAGIC_START = [0x42, 0x4c]
MAGIC_END = 0x00


def headerLength():
    return 2 + 4


def matchMagicHeader(_buffer, _offset):
    if (len(_buffer) < len(MAGIC_START)):
        return False

    if (MAGIC_START[0] == _buffer[_offset] and
        MAGIC_START[1] == _buffer[_offset + 1]):
        return True

    return False


def isGoodLogBuffer(_buffer, _offset, count):
    if _offset == len(_buffer):
        return (True, '')

    if matchMagicHeader(_buffer, _offset):
        headerLen = headerLength()
    else:
        return (False, '_buffer[%d]:%d != MAGIC_NUM_START' % (_offset, _buffer[_offset]))

    if _offset + headerLen + 1 > len(_buffer):
        return (False, 'offset:%d > len(buffer):%d' % (_offset, len(_buffer)))

    length = struct.unpack_from("I", buffer(_buffer, _offset + headerLen - 4, 4))[0]

    if _offset + headerLen + length + 1 > len(_buffer):
        return (False, 'log length:%d, end pos %d > len(buffer):%d' % (length, _offset + headerLen + length + 1, len(_buffer)))

    if MAGIC_END != _buffer[_offset + headerLen + length]:
        return (False, 'log length:%d, buffer[%d]:%d != MAGIC_END' % (length, _offset + headerLen + length, _buffer[_offset + headerLen + length]))

    if (1 >= count):
        return (True, '')
    else:
        return isGoodLogBuffer(_buffer, _offset + headerLen + length + 1, count - 1)


def getLogStartPos(_buffer, _count):
    offset = 0

    while True:
        if offset >= len(_buffer):
            break

        if matchMagicHeader(_buffer, offset):
            if isGoodLogBuffer(_buffer, offset, _count)[0]:
                return offset

        offset += 1
    return -1


def decodeBuffer(_buffer, _offset, _outbuffer):
    if _offset >= len(_buffer):
        return -1

    ret = isGoodLogBuffer(_buffer, _offset, 1)

    if not ret[0]:
        fixpos = getLogStartPos(_buffer[_offset:], 1)
        if -1 == fixpos:
            return -1
        else:
            _outbuffer.extend("[F]decode_log_file.py decode error len=%d, result:%s \n" % (fixpos, ret[1]))
            _offset += fixpos


    if matchMagicHeader(_buffer, _offset):
        headerLen = headerLength()
    else:
        _outbuffer.extend('in DecodeBuffer _buffer[%d]:%d != MAGIC_NUM_START' % (_offset, _buffer[_offset]))
        return -1

    length = struct.unpack_from("I", buffer(_buffer, _offset + headerLen - 4, 4))[0]
    tmpbuffer = bytearray(length)

    tmpbuffer[:] = _buffer[_offset + headerLen: _offset + headerLen + length]

    try:
        decompressor = zlib.decompressobj(-zlib.MAX_WBITS)
        tmpbuffer = decompressor.decompress(str(tmpbuffer))
        _outbuffer.extend(tmpbuffer)
    except Exception, e:
        _outbuffer.extend("[F]decode_log_file.py decompress err, " + str(e) + "\n")

    return _offset + headerLen + length + 1


def parseFile(_file, _outfile):
    fp = open(_file, "rb")
    _buffer = bytearray(os.path.getsize(_file))
    fp.readinto(_buffer)
    fp.close()

    startpos = getLogStartPos(_buffer, 2)
    if -1 == startpos:
        return

    outbuffer = bytearray()

    while True:
        startpos = decodeBuffer(_buffer, startpos, outbuffer)
        if -1 == startpos:
            break

    if 0 == len(outbuffer):
        return

    fpout = open(_outfile, "wb")
    fpout.write(outbuffer)
    fpout.close()


def decompress(fnameList):
    for fname in fnameList:
        output = fname + ".txt"
        parseFile(fname, fname + ".txt")
        print "Decode", fname, "to", output


if __name__ == "__main__":
    decompress(sys.argv[1:])
